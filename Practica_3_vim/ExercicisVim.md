### Practicant amb vim

##### Exercici 1

Com es marca un bloc de línies al vim?

**Resposta: **puedes entrar al modo visual pulsando "v" y después seleccionar la cantidad de líneas que quieras moviéndote línea arriba o abajo con "j" o "k" respectivamente.

##### Exercici 2

Com es copia i s'enganxa el que s'ha marcat a un altre lloc amb el vim?

**Resposta: ** Para copiar has de pulsar la tecla "y" y después colocarte donde lo quieras pegar, pulsa "P" para pegarlo después del cursor o bien "p" para pegarlo antes.

##### Exercici 3

Com t'ho faries per passar de tenir el fitxer repventas.dat a repventas2.dat? (_hint_: un visual diferent dels anteriors)

**Resposta:**  Hemos de situarnos  al inicio de la segunda columna de datos (la que queremos copiar) e introducir el comando Ctrl+v, de manera que entramos en el modo visual de bloque y nos permite seleccionar columnas. Una vez seleccionadas solo tenemos que introducir "y" para copiar y donde queramos "p" para pegar la columna de datos que habíamos seleccionado.

##### Exercici 4

Com ens podem col·locar a una línia concreta? Per exemple la 15.

**Resposta:** Pulsando el número de línea + "G", en este caso "15G"

##### Exercici 5

Com es pot esborrar la línia on es troba el cursor?

**Resposta: **Pulsando dos veces el carácter "d"

##### Exercici 6

Com es poden esborrar 10 línies? (la del cursor i 9 més)

**Resposta:** Podemos insertar "d10G"

##### Exercici 7

Tinc un fitxer amb dues línies ja escrites, amb el vim obro el fitxer i creo una línia buida entre aquestes dues, que és el que passa si, en mode inserció, faig `Ctrl + e`? I si faig `Ctrl + y`?

**Resposta: ** Que va haciendo scroll hacia abajo de línea en línea con Ctrl+e y hacia arriba con Ctrl+y

##### Exercici 8

Estem fent substitució al vim i utilitzem un paràmetre opcional que s'escriu al principi per indicar el rang de línies a les quals afecta la substitució.
Com s'indicaria que aquest rang fos de la línia 1 a la 3? i de la 2 fins al final del fitxer?

**Resposta: ** Tendremos que introducir :1,3s/busqueda/reemplazo/g y :2,$s/busqueda/reemplazo/g


##### Exercici 9

El fitxer _Secret.txt_ s'ha corromput. Afortunadament s'ha detectat que on hi ha un _pitostu_ hauria d'haver una _e_ i just després on hi ha un _kekew_ hi hauria d'haver una _a_.
Un cop fet l'exercici no llegeixis el text sisplau.

**Resposta:** Primero hacemos :%s/pitostu/e/g y luego :%s/kekew/a/g y ya estaría

##### Exercici 10

El fitxer _sonet.txt_ també s'ha corrumput, de fet es tracta d'un sonet i s'han barrejat certs blocs.
Heu de trobar l'ordre original utilitzant _delete/paste_ en mode _Visual_ i al menys un parell de registres/buffers. A més volem eliminar de _manera elegant_ les línies buides.

Segurament ja hareu mirat a la wikipedia l'estructura d'un sonet:

```
primer cuartet
segon cuartet
primer tercet
segon tercet
```
**Resposta:** He cortado y pegado los diferentes bloques seleccionándolos en modo visual y cortándolos con "d" y pegándolos con "p" en el lugar que les corresponde. Por último he eliminado las líneas en blanco que quedaban entre los versos con el comando "dd" que permite eliminar la línea entera.

##### Exercici 11

Com podem passar a majuscules un text que està marcat en mode visual? I a minúscules?

**Resposta:**  Con el comando "~" se cambia la letra de minúscula a mayúscula y viceversa.


##### Exercici 12

Si edito un fitxer de nom _.vimrc_ al meu _$HOME_ i el seu contingut tingués:

```
set tabstop=4
set number
set autoindent
```

Quines serien les *conseqüències*?

**Resposta: ** Con set number hacemos que se muestren los números de línea a la izquierda, con tabstop le estamos indicando el número de espacios de los que consta una tabulación, en este caso 4; y con set autoindent  le estamos diciendo que queremos indentación automática en el documento que escribamos.

##### Exercici 13

Si estic en mode ordre, quina tecla (o combinació de tecles) desfà l'últim canvi fet? i si estic en mode inserció? (en anglès es diu _undo_ _un-do_)

**Resposta:**  El comando es "u" y en el modo inserción pues será necesario salir con Esc y entonces usar el comando "u"

##### Exercici 14 

I quina tecla (o combinació de tecles) torna a deixar les coses com estaven? És a dir a des-fer el canvi en mode ordre? (en anglès es diu "redo": re-do, "undo the undos")

**Resposta:** con la combinación Ctrl+r

##### Exercici 15

És possible executar una ordre del bash si estem a dintre del vim? Si la resposta és afirmativa com ho podríem fer?

**Resposta:** Se puede ejecutar una orden escribiendo _:w !_ delante del comando que quera os ejecutar

##### Exercici 16

`vim` i `vi` no són el mateix. `vim` és una versió millorada (IMproved) de vi. Però a Fedora és el mateix `vi` que `vim`? Que diuen `type`, `which` o `whereis` d'aquestes dues ordres (`vi` i `vim`)?

És el mateix executar `vi`, `vim` o `/usr/bin/vi`? És a dir si fem:

```
vi Hello.java
```
i dintre posem:
```
public class Hello {
  public static void main(String] args) {
		  System.out.println("Hello World");
  }
}
```
veiem alguna diferència amb:
```
vim Hello.java
```
i amb
```
/usr/bin/vi  Hello.java
```
?

**Resposta:** No hay ninguna diferencia, de hecho vim tiene un alias hecho para que cuando escribimos el comando vim sea como vim=vi.

##### Exercici 17 

Volem comentar un bloc de línies amb el vim. Com ho podem fer? (Hint: Bloc Visual + r]eplace)

**Resposta:** Debemos entrar en el modo bloque visual con "V" y seleccionar la primera columna de todas las líneas que queramos. Entonces entramos en el modo inserción con "I" y escribimos # o // o la sintaxis de comentario necesaria.

##### Exercici 18

Volem afegir un exercici a una llista d'exercicis de vim escrita en text pla (markdown), malauradament el nou exercici està al mig de la llista i per tant haurem de modificar la numeració dels exercicis. Existeix una combinació de tecles en mode ordre que incrementi un número en una unitat? I que el decrementi? Existeix algun caràcter en mode ordre que repeteixi la darrera instrucció executada al vim?
Per exemple, com ho faries per numerar correctament aquest text combinant els comentat abans?:

```
###### Exercici 1
assssssssssssssssssssssssss
assssssssssssssssssssssssssaaaa

###### Exercici 1
asssssssssssssssssssssss
###### Exercici 2
aa
###### Exercici 3
as
as
as
###### Exercici 4

###### Exercici 5
```

**Resposta:** Lo que yo haría sería entrar en modo visual de bloque (Ctrl+V) y seleccionar la columna donde se indica los números de ejercicio excepto en el primer enunciado de Exercici 1, y cuando tengamos toda esa columna seleccionada ejecutamos "Ctrl+a" para incrementar todos los números de ejercicio en una unidad.