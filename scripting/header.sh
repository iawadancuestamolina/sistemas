#!/bin/bash
# Filename:	header.sh
# Author:	Adán Cuesta Molina
# Date:		22/11/2020
# Version:	0.2
# License:	This is free software, licensed under the GNU General Public License v3.
# 		See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:	./header.sh  script_name.sh
# Description:	Script to automate the creation of headers.
# 		This program will get the name of the script from the command line
# 		and it will create the header details, with execute permission,
# 		and, finally, the developer will run vim in insert mode.


license="This is free software, licensed under the GNU General Public License v3.
#        See http://www.gnu.org/licenses/gpl.html for more information."

echo -e "

#!/bin/bash
# Filename:	$1
# Author:	$(whoami)
# Date:	    $(date +"%d-%m-%y")
# Version:	0.1
# License:  $license	
# Usage:    $1 [arg1...]	
# Description:" > $1
chmod u+x "$1"
vim "$1" + -c start


