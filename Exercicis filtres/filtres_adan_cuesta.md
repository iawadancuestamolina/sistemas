
### 1. Donats els dos fitxers “repventas.dat” i “oficinas.dat”, vull saber quin és exactament el caràcter delimitador de camps utilitzat a aquests fitxers. 
### Amb un editor com cat, less o more no puc distingir un espais en blanc d'un tabulador. Però al tema que estem treballant actualment hi ha una ordre que em permet veure de quins caràcters exactament està compost un fitxer. En particular podré saber quin és el caràcter delimitador de camps (“columnes”).
###Troba l'ordre i escriu l'ordre i el resultat que demostra quin és el delimitador per a “repventas.dat”. Anàlogament per a l'altre fitxer.

**Respuesta:** 
```
[fanelli@Macbook Exercicis filtres]$ cat repventas.dat | od -c
0000000   1   0   5  \t   B   i   l   l       A   d   a   m   s  \t   3
0000020   7  \t   1   3  \t   R   e   p       V   e   n   t   a   s  \t
0000040   1   9   8   8   -   0   2   -   1   2  \t   1   0   4  \t   3
0000060   5   0   0   0   0  \t   3   6   7   9   1   1  \n   1   0   9
0000100  \t   M   a   r   y       J   o   n   e   s  \t   3   1  \t   1
0000120   1  \t   R   e   p       V   e   n   t   a   s  \t   1   9   8
0000140   9   -   1   0   -   1   2  \t   1   0   6  \t   3   0   0   0
0000160   0   0  \t   3   9   2   7   2   5  \n   1   0   2  \t   S   u
0000200   e       S   m   i   t   h  \t   4   8  \t   2   1  \t   R   e
0000220   p       V   e   n   t   a   s  \t   1   9   8   6   -   1   2
0000240   -   1   0  \t   1   0   8  \t   3   5   0   0   0   0  \t   4
0000260   7   4   0   5   0  \n   1   0   6  \t   S   a   m       C   l
0000300   a   r   k  \t   5   2  \t   1   1  \t   V   P       V   e   n
0000320   t   a   s  \t   1   9   8   8   -   0   6   -   1   4  \t   \
0000340   N  \t   2   7   5   0   0   0  \t   2   9   9   9   1   2  \n
0000360   1   0   4  \t   B   o   b       S   m   i   t   h  \t   3   3
0000400  \t   1   2  \t   D   i   r       V   e   n   t   a   s  \t   1
0000420   9   8   7   -   0   5   -   1   9  \t   1   0   6  \t   2   0
0000440   0   0   0   0  \t   1   4   2   5   9   4  \n   1   0   1  \t
0000460   D   a   n       R   o   b   e   r   t   s  \t   4   5  \t   1
0000500   2  \t   R   e   p       V   e   n   t   a   s  \t   1   9   8
0000520   6   -   1   0   -   2   0  \t   1   0   4  \t   3   0   0   0
0000540   0   0  \t   3   0   5   6   7   3  \n   1   1   0  \t   T   o
0000560   m       S   n   y   d   e   r  \t   4   1  \t   \   N  \t   R
0000600   e   p       V   e   n   t   a   s  \t   1   9   9   0   -   0
0000620   1   -   1   3  \t   1   0   1  \t   \   N  \t   7   5   9   8
0000640   5  \n   1   0   8  \t   L   a   r   r   y       F   i   t   c
0000660   h  \t   6   2  \t   2   1  \t   D   i   r       V   e   n   t
0000700   a   s  \t   1   9   8   9   -   1   0   -   1   2  \t   1   0
0000720   6  \t   3   5   0   0   0   0  \t   3   6   1   8   6   5  \n
0000740   1   0   3  \t   P   a   u   l       C   r   u   z  \t   2   9
0000760  \t   1   2  \t   R   e   p       V   e   n   t   a   s  \t   1
0001000   9   8   7   -   0   3   -   0   1  \t   1   0   4  \t   2   7
0001020   5   0   0   0  \t   2   8   6   7   7   5  \n   1   0   7  \t
0001040   N   a   n   c   y       A   n   g   e   l   l   i  \t   4   9
0001060  \t   2   2  \t   R   e   p       V   e   n   t   a   s  \t   1
0001100   9   8   8   -   1   1   -   1   4  \t   1   0   8  \t   3   0
0001120   0   0   0   0  \t   1   8   6   0   4   2  \n
0001134
```
Parece que el carácter delimitador es \t es decir un tabulador y lo mismo sucede con el archivo oficinas.dat:

```
[fanelli@Macbook Exercicis filtres]$ cat oficinas.dat | od -c
0000000   2   2  \t   D   e   n   v   e   r  \t   O   e   s   t   e  \t
0000020   1   0   8  \t   3   0   0   0   0   0  \t   1   8   6   0   4
0000040   2  \n   1   1  \t   N   e   w       Y   o   r   k  \t   E   s
0000060   t   e  \t   1   0   6  \t   5   7   5   0   0   0  \t   6   9
0000100   2   6   3   7  \n   1   2  \t   C   h   i   c   a   g   o  \t
0000120   E   s   t   e  \t   1   0   4  \t   8   0   0   0   0   0  \t
0000140   7   3   5   0   4   2  \n   1   3  \t   A   t   l   a   n   t
0000160   a  \t   E   s   t   e  \t   1   0   5  \t   3   5   0   0   0
0000200   0  \t   3   6   7   9   1   1  \n   2   1  \t   L   o   s    
0000220   A   n   g   e   l   e   s  \t   O   e   s   t   e  \t   1   0
0000240   8  \t   7   2   5   0   0   0  \t   8   3   5   9   1   5  \n
0000260
```


### 2. Estudiem l'ordre cut. Aquesta ens mostra seccions de cada línia dels fitxers. En particular, si un fitxer està ben ordenat, amb camps(columnes) i delimitadors (un caràcter que fa de separador de camps), podem fer que ens mostri les columnes que volguem d'un cert fitxer.
###Quin és el paràmetre que em serveix per indicar el caràcter delimitador? Si no poso res quin és el caràcter que agafa per defecte com a delimitador (en anglès delimiter) ? Quin és el paràmetre que indica per quina columna o camp (en anglès field) tallem el text o sigui el mostrem?
### Mostra el camp 1 del fitxer oficinas.dat.
### Mostra el camp 1 i el 2.
### Mostra el camp 1 i el 3, 4 i 5
### Suposa que no saps quants camps hi ha, mostra tots els camps menys el 2.

**Resposta:** El carácter delimitador por defecto es el tabulador (\t). Para escoger otro carácter delimitador que no sea el que se coge por defecto hay que poner el parámetro -d y para indicar que columnas cogemos del texto se usa -f.

```
[fanelli@Macbook Exercicis filtres]$ cut -f1 oficinas.dat 
22
11
12
13
21

[fanelli@Macbook Exercicis filtres]$ cut -f1,2 oficinas.dat 
22	Denver
11	New York
12	Chicago
13	Atlanta
21	Los Angeles

[fanelli@Macbook Exercicis filtres]$ cut -f1-5 oficinas.dat 
22	Denver	Oeste	108	300000
11	New York	Este	106	575000
12	Chicago	Este	104	800000
13	Atlanta	Este	105	350000
21	Los Angeles	Oeste	108	725000

[fanelli@Macbook Exercicis filtres]$ cut -f-1,3- oficinas.dat 
22	Oeste	108	300000	186042
11	Este	106	575000	692637
12	Este	104	800000	735042
13	Este	105	350000	367911
21	Oeste	108	725000	835915
```



### 3. Amb quina ordre podria saber les oficines que tinc ? He de suposar que per cada línia, o sigui per cada \n, hi haurà una oficina.

**Resposta:** Podemos contar el número de líneas con el comando wc (la primera cifra nos da el número de líneas)
       
       
### 4. Volem canviar el delimitador que hi ha a oficinas.dat pel caràcter “:”, com ho faries ?
### I fer aquest canvi i a més que el nou fitxer tingui com a nom “oficinas.txt” ? Resoleu aquest exercici amb 2 ordres diferents. (Feu el mateix amb repventas.dat) 

**Resposta:** 

```
[fanelli@Macbook Exercicis filtres]$ sed 's/\t/:/g' oficinas.dat > oficinas.txt

[fanelli@Macbook Exercicis filtres]$ tr \\t : < repventas.dat > repventas.txt

```


       
### 5. Mostreu ara els camps 1 i 3 del nou fitxer oficinas.txt

**Resposta:**

```
[fanelli@Macbook Exercicis filtres]$ cut -f1,3 -d: oficinas.txt 
22:Oeste
11:Este
12:Este
13:Este
21:Oeste
```
       
       
### 6. Mostreu les possibles regions que hi pot tenir una oficina. Òbviament no hi ha d'haver duplicats.
### Si busqueu al man adient trobareu al menys dues possibles maneres de fer això. (Si no ordeneu primer, no us anirà bé)

**Resposta:**
```
[fanelli@Macbook Exercicis filtres]$ cut -f3 -d: oficinas.txt | sort -u
Este
Oeste
```

       
       
### 7. Ordena repventas.txt pel 3er camp. Un cop trobis l'ordre adient, fixat que necessites passar-li alguna cosa més que el camp pel que vols ordenar.

**Resposta:** 
```
[fanelli@Macbook Exercicis filtres]$ sort -t: -k3 repventas.txt
103:Paul Cruz:29:12:Rep Ventas:1987-03-01:104:275000:286775
109:Mary Jones:31:11:Rep Ventas:1989-10-12:106:300000:392725
104:Bob Smith:33:12:Dir Ventas:1987-05-19:106:200000:142594
105:Bill Adams:37:13:Rep Ventas:1988-02-12:104:350000:367911
110:Tom Snyder:41:\N:Rep Ventas:1990-01-13:101:\N:75985
101:Dan Roberts:45:12:Rep Ventas:1986-10-20:104:300000:305673
102:Sue Smith:48:21:Rep Ventas:1986-12-10:108:350000:474050
107:Nancy Angelli:49:22:Rep Ventas:1988-11-14:108:300000:186042
106:Sam Clark:52:11:VP Ventas:1988-06-14:\N:275000:299912
108:Larry Fitch:62:21:Dir Ventas:1989-10-12:106:350000:361865
```

### 8. Feu el mateix per ordenar repventas.dat pel 3er camp. (Aquesta és moooolt difícil)

**Resposta:** 
```
[fanelli@Macbook Exercicis filtres]$ sort -k4 repventas.dat
103	Paul Cruz	29	12	Rep Ventas	1987-03-01	104	275000	286775
109	Mary Jones	31	11	Rep Ventas	1989-10-12	106	300000	392725
104	Bob Smith	33	12	Dir Ventas	1987-05-19	106	200000	142594
105	Bill Adams	37	13	Rep Ventas	1988-02-12	104	350000	367911
110	Tom Snyder	41	\N	Rep Ventas	1990-01-13	101	\N	75985
101	Dan Roberts	45	12	Rep Ventas	1986-10-20	104	300000	305673
102	Sue Smith	48	21	Rep Ventas	1986-12-10	108	350000	474050
107	Nancy Angelli	49	22	Rep Ventas	1988-11-14	108	300000	186042
106	Sam Clark	52	11	VP Ventas	1988-06-14	\N	275000	299912
108	Larry Fitch	62	21	Dir Ventas	1989-10-12	106	350000	361865
```

       
### 9. Ara que ja heu aprés a ordenar fitxers, ordeneu repventas.txt pel 4rt camp (no de forma numèrica, que si no la lia amb el NULL) i deseu-lo com a repventasOrd.txt. Feu el mateix amb oficinas.txt pel primer camp i deseu-lo a oficinasOrd.txt.

**Resposta:** 
```

[fanelli@Macbook Exercicis filtres]$ sort -g -k4 -t: repventas.txt>repventasOrd.txt


[fanelli@Macbook Exercicis filtres]$ sort -k1 oficinas.txt > oficinasOrd.txt
```

       
### 10. Ara que teniu 2 fitxers que tenen 2 camps en comú i que a més estan ordenats, podem fer servir el join per unir els 2 fitxers pel camp que tenen en comú. Per a a això hem d'indicar el delimitador i els camps de cada fitxer que volem unir.
**Resposta:**Me ha sido imposible averiguar cómo ejecutar join con un campo \N

### 11. Suposeu que teniu algun problema amb el reconeixement del vostre pen USB quan el connecteu amb el vostre Fedora 20. En anteriors versions de del sistema operatiu es podia llegir el fitxer /var/log/messages, que era el principal fitxer de servei-dimoni de logs, syslogd. L'ordre per llegir «en directe» era:   tail -f /var/log/messages
### Ara el dimoni que es fa servir és journald. I amb l'ordre adequada es poden llegir els missatges del sistema.
### Digues quina és l'ordre que:
### - permet llegir els missatges del sistema
### - permet llegir en directe els missatges del sistema
### - permet llegir els missatges d'arrencada (boot)
       
### (Per fer la 2a ordre executa la mateixa ordre a la consola i després connecta el pen USB, observa els missatges que surten)
       
**Resposta:** La orden para leer los mensajes del sistema es journalctl. Para leerlos en directo podemos ejecutar journalctl -f  y para leer los mensajes de boot  journalctl --list-boots

       
### 12. Amb l'ordre split algú ha dividit un fitxer en 3 parts (file1, file2 i file3), però sembla que després s'han canviat els noms dels fitxers, de manera que no estan ordenats. Heu de descobrir, a pèl, quin és l'ordre en el qual s'han d'ajuntar aquests trossos de fitxers perquè puguem obtenir el fitxer original. 
### Un cop descobriu això, torneu a partir el fitxer però ara volem que els 3 fitxers resultants siguin de 10000000 de bytes (aproximadament 10 MB) i que els noms siguin: file00, file01, file02 
       
### OBS: els fitxers  file1, file2 i file3 els teniu a public.

**Resposta:** 
```
[fanelli@Macbook Exercicis filtres]$ cat Exercicis_Filtres_splitFile_file2.webm Exercicis_Filtres_splitFile_file3 Exercicis_Filtres_splitFile_file1 > video.webm
*El orden simplemente lo supuse porque era el más inverosímil posible y si no hubiese acertado solo habría una posibilidad más que intentar, ya que está claro que el fichero .webm es el inicial*

[fanelli@Macbook Exercicis filtres]$ split -b 10000000 -d video.webm file
```



