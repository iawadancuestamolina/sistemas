## Practica 1 Sistemes Informatics

### En quin directori us trobeu?

/media/discohd/Documents/Desarrollo Aplicaciones Web/M01_Sistemes_Informatics/practica1

### Quines són les ordres que heu emprat per crear aquesta estructura de directoris?

`mkdir practica1`
`cd practica1`
`mkdir -p subdirectori1/{subdirectori11,subdirectori12}`
`mkdir -p subdirectori2/{subdirectori21,subdirectori22/{subdirectori221,subdirectori222}}`

### Hi ha algun paràmetre que permeti crear més d'un directori de cop?

Si, el argumento -p del comando mkdir
