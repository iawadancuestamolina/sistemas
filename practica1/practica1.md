## Practica 1 Sistemes Informatics

### En quin directori us trobeu?

/media/discohd/Documents/Desarrollo Aplicaciones Web/M01_Sistemes_Informatics/practica1

### Quines són les ordres que heu emprat per crear aquesta estructura de directoris?

`mkdir practica1`
`cd practica1`
`mkdir -p subdirectori1/{subdirectori11,subdirectori12}`
`mkdir -p subdirectori2/{subdirectori21,subdirectori22/{subdirectori221,subdirectori222}}`

### Hi ha algun paràmetre que permeti crear més d'un directori de cop?

Si, el argumento -p del comando mkdir

### Col·loqueu-vos al vostre home, editeu amb vim un fitxer anonomenat fitxerA dintre del directori subdirectori222 i deseu-lo.

`vim M01_Sistemes_Informatics/subdirectori2/subdirectori22/`
`touch archivo.txt`


### Copieu-lo a subdirectori12 (indiqueu a on us trobeu)

`cp archivo.txt M01_Sistemes_Informatics/subdirectori2/`

### Canvieu el nom del fitxerA que es troba a subdirectori222 per fitxerB

`cd /media/discohd/Documents/Desarrollo Aplicaciones Web/M01_Sistemes_Informatics/practica1/subdirectori2/subdirectori22/subdirectori222/`
`mv archivo.txt fitxerB.txt`

### Intenteu eliminar el subdirectori222 executant l'ordre: rm subdirectori222 Que passa? No feu res, però digueu quina seria la solució?

No deja, porque estamos dentro, para eliminarlo habria que salir del directorio

### Moveu el fitxer fitxerB que es troba a subdirectori222 al subdirectori12.

`mv fitxerB.txt /media/discohd/Documents/Desarrollo Aplicaciones Web/M01_Sistemes_Informatics/practica1/subdirectori12/`

